const jwt = require('jsonwebtoken');

const secret = 'EcommerceBackEndAPI';

//Create access token upon login
module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };
    return jwt.sign(data, secret, {})
}

//Verification upon browsing endpoints
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;

    if(typeof token !== "undefined"){
        console.log(token);
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {
            if(err){
                return res.send({auth: "failed"});
            }else{
                next();
            }
        })
    }else{
        return res.send({auth: "failed"});
    }
}

//decoding upon verify the token login
module.exports.decode = (token) => {
    if(typeof token !== "undefined"){
        token = token.slice(7, token.length);
        return jwt.verify(token, secret, (err, data) => {
            if(err){
                return null;
            }else{
                return jwt.decode(token, {complete:true}).payload
            }
        })
    }else{
        return null;
    }
}


/*const verifyToken = (req, res, next) => {
    const authHeader = req.headers.authorization;
    if(authHeader){
        const token = authHeader.split(" ");
        jwt.verify(token, (err, user) => {
            if(err){
                return false;
            }
            req.user = user;
            next();
        })
    }else{
        return res.status(401).json("you are not authenticated!")
    }
}*/

// module.exports = {verifyToken}