import { useContext, useEffect} from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext'

const Logout = () => {

    const { setUser , unsetUser} = useContext(UserContext)

    unsetUser();

    useEffect(() => {
        setUser({ accessToken: null })
    }, [])

    return (
        <Redirect to="/login" />
    )
}

export default Logout
