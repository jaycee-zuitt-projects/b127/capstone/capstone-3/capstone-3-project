import { Fragment, useState, useContext, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

const Register = () => {

    const history = useHistory();

    const { user, setUser } = useContext(UserContext)

    //states
    const [ firstName, setFirstName ] = useState("");
    const [ lastName, setLastName ] = useState("");
    const [ email, setEmail ] = useState("");
    const [ address, setAddress ] = useState("");
    const [ mobileNo, setMobileNo ] = useState("");
    const [ password, setPassword ] = useState("");
    const [ password2, setPassword2 ] = useState("");

    //button state
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if((firstName !== "" && lastName !== "" && email !== "" && address !== "" && mobileNo !== "" && password !== "" && password2 !== "") && (mobileNo >= 11) && (password === password2)){
            setIsActive(true);
        }else{
            setIsActive(false);
        } 
    }, [firstName, lastName, email, address, mobileNo, password, password2])

    function registerUser(e){
        e.preventDefault();

        fetch(`${ process.env.REACT_APP_API_URL }/users/verifyEmail`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true){
                Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'error',
                    text: "Email is alreardy exist."
                })
            }else{
                fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        address: address,
                        mobileNo: mobileNo,
                        password: password
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    if(data === true){
                        Swal.fire({
                            title: "You are now registered!",
                            icon: 'success',
                            text: 'Successfully registered, enjoy shopping.'
                        })

                        history.push("/login");

                    }else{
                        Swal.fire({
                            title: "Something Went Wrong",
                            icon: 'error',
                            text: 'Please check the input fields.'
                        })
                    }
                })
                    setFirstName("");
                    setLastName("");
                    setEmail("");
                    setAddress("");
                    setMobileNo("");
                    setPassword("");
                    setPassword2("");
            }
        })
    }


    return (
        (user.accessToken !== null)?
        <Redirect to="/" />
        :
        <Fragment>
            <h1>User Registration</h1>
            <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group>
                    <Form.Label>First Name:</Form.Label>
                    <Form.Control 
                    type="text" 
                    placeholder="Enter Your First Name" 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Last Name:</Form.Label>
                    <Form.Control 
                    type="text" 
                    placeholder="Enter Your Last Name" 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Email Address:</Form.Label>
                    <Form.Control 
                    type="text" 
                    placeholder="Enter Your Email Address" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Address:</Form.Label>
                    <Form.Control 
                    type="text" 
                    placeholder="Enter Your Address" 
                    value={address}
                    onChange={e => setAddress(e.target.value)}
                    required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Mobile Number:</Form.Label>
                    <Form.Control 
                    type="number" 
                    placeholder="Enter Your Mobile Number" 
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                    required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password:</Form.Label>
                    <Form.Control 
                    type="password" 
                    placeholder="Enter Your Password" 
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
                    type="password" 
                    placeholder="Re-Enter Your Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                    />
                </Form.Group>
                {isActive ?
                <Button variant="primary" type="submit">Register</Button>
                :
                <Button variant="primary" type="submit" disabled>Register</Button>
                }
            </Form>
        </Fragment>
    )
}

export default Register
