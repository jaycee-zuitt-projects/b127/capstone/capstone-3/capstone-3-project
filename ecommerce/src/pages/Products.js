import { useState, useEffect, useContext } from 'react';

//bootstrap
import { Container } from 'react-bootstrap';
//components
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';

//react context
import UserContext from '../UserContext';

const Product = () => {
    const { user } = useContext(UserContext)

    const [ allProduct, setAllProduct ] = useState([])

    const fetchData = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/allusion/allProducts`)
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setAllProduct(data)
        })
    }
    
    useEffect(() => {
        fetchData()
    }, [])

    return(
        <Container>
            {
                (user.isAdmin === true)?
                <AdminView productData={allProduct} fetchData={fetchData} />
                :
                <UserView productData={allProduct} />
            }
        </Container>
    )


}

export default Product;