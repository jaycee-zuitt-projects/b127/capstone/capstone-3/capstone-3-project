import { Fragment, useContext } from 'react';
import { Navbar, Nav, Form } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
//React Context
import UserContext from '../UserContext';

export default function AppNavbar(){
    const { user } = useContext(UserContext)

    let rightNav = (user.accessToken !== null)
    ?
    <Fragment>
        <Nav.Link as={NavLink} to="/logout"> Logout </Nav.Link>
    </Fragment>
    :
    <Fragment>
        <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
		<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
    </Fragment>


    return(
        <Navbar bg="dark" variant="dark" expand="lg">
            <Navbar.Brand as={Link} to="/" id="logo">Allusion Ecom Shop</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Form className="d-flex">
            </Form>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
                    {rightNav}
                    
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}